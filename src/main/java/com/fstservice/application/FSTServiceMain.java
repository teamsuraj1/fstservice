package com.fstservice.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(exclude =
{ MongoAutoConfiguration.class, MongoDataAutoConfiguration.class, SolrAutoConfiguration.class })
public class FSTServiceMain extends SpringBootServletInitializer
{
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
	{
		return application.sources(FSTServiceMain.class).properties("spring.config.name:FSTServiceMain");
	}
	
	public static void main(String[] args) throws Exception
	{
		SpringApplication.run(FSTServiceMain.class, args);
	}
	
}