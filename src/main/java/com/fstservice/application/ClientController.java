package com.fstservice.application;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.hyperledger.fabric.gateway.Contract;
import org.hyperledger.fabric.gateway.Gateway;
import org.hyperledger.fabric.gateway.Network;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(maxAge = 3600)
@RestController
@Api(value = "Controller for the Client App", description = "Service for Interacting with Farm Supply Ledger")
public class ClientController
{
	
	@Autowired
	EnrollAdminAndUser	walletController;
	
	private String		connectionOrg1Path	= "/home/teamsurajgmail/fabric-samples/test-network/organizations/peerOrganizations/org1.example.com/connection-org1.yaml";
	private String		channelName			= "mychannel";
	private String		contractName		= "Trackfarmsupply";
	
	// @formatter:off
	@RequestMapping(method = RequestMethod.POST, value = "/addFarmProduct")
	@ApiOperation(value = "Adding the Farm Product To Ledger", notes = "Adding the Farm Product To Ledger")
	@ApiResponses(value =
	{ @ApiResponse(code = 200, message = "Ok"), @ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 412, message = "Precondition failed"), @ApiResponse(code = 424, message = "Failed Dependency"), @ApiResponse(code = 500, message = "Internal Server Error") })
	private String addFarmProduct(String _id, String _description, String _producerName, String _producerAddress, String _harvestDate) throws Exception
	{
		
		Path networkConfigPath = Paths.get(connectionOrg1Path);
		
		Gateway.Builder builder = Gateway.createBuilder();
		builder.identity(walletController.getWallet(), "appUser").networkConfig(networkConfigPath).discovery(true);
		
		try (Gateway gateway = builder.connect())
		{
			Network network = gateway.getNetwork(channelName);
			Contract contract = network.getContract(contractName);
			contract.submitTransaction("addProduct", _id, _description, _producerName, _producerAddress, _harvestDate);
			byte[] result = contract.evaluateTransaction("getFarmProduct", _id);
			
			return (new String(result));
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/transferToDistributer")
	@ApiOperation(value = "Transfer Asset from Producer to Distributer", notes = "Transfer Asset from Producer to Distributer")
	@ApiResponses(value =
	{ @ApiResponse(code = 200, message = "Ok"), @ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 412, message = "Precondition failed"), @ApiResponse(code = 424, message = "Failed Dependency"), @ApiResponse(code = 500, message = "Internal Server Error") })
	private String transferToDistributer(String _id, String distributerName, String distributerAddress, String transferDate) throws Exception
	{
		
		Path networkConfigPath = Paths.get(connectionOrg1Path);
		
		Gateway.Builder builder = Gateway.createBuilder();
		builder.identity(walletController.getWallet(), "appUser").networkConfig(networkConfigPath).discovery(true);
		try (Gateway gateway = builder.connect())
		{
			Network network = gateway.getNetwork(channelName);
			Contract contract = network.getContract(contractName);
			contract.submitTransaction("assignDistributer", _id, distributerName, distributerAddress, transferDate);
			byte[] result = contract.evaluateTransaction("getFarmProduct", _id);
			
			return (new String(result));
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/transferToRetailer")
	@ApiOperation(value = "Transfer Asset from Distributer to Retailer", notes = "Transfer Asset from Distributer to Retailer")
	@ApiResponses(value =
	{ @ApiResponse(code = 200, message = "Ok"), @ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 412, message = "Precondition failed"), @ApiResponse(code = 424, message = "Failed Dependency"), @ApiResponse(code = 500, message = "Internal Server Error") })
	private String transferToRetailer(String _id, String retailerName, String retailerAddress, String transferDate) throws Exception
	{
		
		Path networkConfigPath = Paths.get(connectionOrg1Path);
		
		Gateway.Builder builder = Gateway.createBuilder();
		builder.identity(walletController.getWallet(), "appUser").networkConfig(networkConfigPath).discovery(true);
		try (Gateway gateway = builder.connect())
		{
			Network network = gateway.getNetwork(channelName);
			Contract contract = network.getContract(contractName);
			contract.submitTransaction("transferToRetailer", _id, retailerName, retailerAddress, transferDate);
			byte[] result = contract.evaluateTransaction("getFarmProduct", _id);
			
			return (new String(result));
		}
		catch (Exception ex)
		{
			throw ex;
		}
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getFarmProduct")
	@ApiOperation(value = "Querying a particular Product", notes = "Querying a particular Product")
	@ApiResponses(value =
	{ @ApiResponse(code = 200, message = "Ok"), @ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 412, message = "Precondition failed"), @ApiResponse(code = 424, message = "Failed Dependency"), @ApiResponse(code = 500, message = "Internal Server Error") })
	private String getFarmProduct(String _id) throws Exception
	{
		Path networkConfigPath = Paths.get(connectionOrg1Path);
		
		Gateway.Builder builder = Gateway.createBuilder();
		builder.identity(walletController.getWallet(), "appUser").networkConfig(networkConfigPath).discovery(true);
		try (Gateway gateway = builder.connect())
		{
			Network network = gateway.getNetwork(channelName);
			Contract contract = network.getContract(contractName);
			byte[] result = contract.evaluateTransaction("getFarmProduct", _id);
			
			return (new String(result));
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}
	
}
